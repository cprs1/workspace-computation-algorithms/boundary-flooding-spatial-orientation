%% BOUNDARY FLOODING ALGORITHM
% F.Zaccaria 02 February 2022


%% INPUT FILE

%% Simulation parameters

maxiter = 20;
TOL = 10^-5;
TOL2 = Inf;
stepsize_x = 2*pi/180;
stepsize_y = 2*pi/180;
stepsize_z = 2*pi/180;
boxsize = [-180 180 1 +360 -1 1]*pi/180;

n_rad = 2^2;
tau = 300;

figure()
grid minor
axis equal
hold on
axis(boxsize)
xlabel('\alpha')
ylabel('\beta')
zlabel('\gamma')
%% STORE VARIABLES

params.n_rad = n_rad;
params.stepsize_x = stepsize_x;
params.stepsize_y = stepsize_y;
params.stepsize_z = stepsize_z;
params.maxiter = maxiter;
params.boxsize = boxsize;
params.TOL = TOL;
params.TOL2 = TOL2;
params.tau = tau;