function v = invskew(M)

v = zeros(3,1);
v(1,1) = -M(2,3);
v(2,1) = +M(1,3);
v(3,1) = -M(1,2);
end