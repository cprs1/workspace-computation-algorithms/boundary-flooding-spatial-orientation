%% BACKWARD ODEs of a single beam
% for assumed strain mode approach
function yd = OdefunAssumeBackward(s,y,qei,~,Nf,L,str)
Nftot = 3*Nf;

gamma = y(1:6,1);
dgammadqa = y(1+6+Nftot:6+Nftot+6,1);
dgammadqe = reshape(y(1+6+Nftot+6+Nftot:6+Nftot+6+Nftot+6*Nftot,1),6,Nftot);
dgammadwL = reshape(y(1+6+Nftot+6+Nftot+Nftot*(6+Nftot):6+Nftot+6+Nftot+Nftot*(6+Nftot)+6*6,1),6,6);
Phi = PhiMatr(s,L,Nf);
B = [eye(3);zeros(3)];       
k = Phi*qei;
sk = [0,-k(3),+k(2);+k(3),0,-k(1);-k(2),+k(1),0];
s1 = [0,-1,0;1,0,0;0,0,0];
adxi = [sk, zeros(3); s1 , sk];
matderiv = [+Phi(3,:)*gamma(2)-Phi(2,:)*gamma(3);
            -Phi(3,:)*gamma(1)+Phi(1,:)*gamma(3);
            +Phi(2,:)*gamma(1)-Phi(1,:)*gamma(2);
            +Phi(3,:)*gamma(5)-Phi(2,:)*gamma(6);
            -Phi(3,:)*gamma(4)+Phi(1,:)*gamma(6);
            +Phi(2,:)*gamma(4)-Phi(1,:)*gamma(5)];
        
%% these infos should be provided by forward integration
% ys = deval(solf,s);
% hs = ys(4:7,1);
% Rs = quat2rotmatrix(hs);
% Adgfinv = [Rs', zeros(3) ; zeros(3), Rs'];
% wd = Adgfinv*wd; 

% you need it also for the derivatives of the gravitational terms for the
% jacobian
dwddqa = zeros(6,1);
dwddqe = zeros(6,Nftot);
wd = zeros(6,1);
%% 
switch str
    case 'fix'
        gammad = adxi'*gamma -wd;
        Qcd = (B*Phi)'*gamma;
        dgammadqad = adxi'*dgammadqa -dwddqa;
        dgammadqed = matderiv + adxi'*dgammadqe -dwddqe;
        dgammadwLd = adxi'*dgammadwL;
        dQcdqad = (B*Phi)'*dgammadqa;
        dQcdqed = (B*Phi)'*dgammadqe;
        dQcdwLd = (B*Phi)'*dgammadwL;
        
        yd = L*[gammad;Qcd;dgammadqad;dQcdqad;reshape(dgammadqed,6*Nftot,1);reshape(dQcdqed,Nftot*Nftot,1);reshape(dgammadwLd,6*6,1);reshape(dQcdwLd,6*Nftot,1)];

    case 'variable'
        gammad = L*adxi'*gamma -wd;
        Qcd = L*(B*Phi)'*gamma;
        dgammadqad = L*(adxi'*dgammadqa -dwddqa) + adxi'*gamma -wd;
        dgammadqed = L*(matderiv + adxi'*dgammadqe -dwddqe);
        dgammadwLd = L*(adxi'*dgammadwL);
        dQcdqad = (B*Phi)'*(L*dgammadqa+gamma) ;
        dQcdqed = L*(B*Phi)'*dgammadqe;
        dQcdwLd = L*(B*Phi)'*dgammadwL;
        yd = [gammad;Qcd;dgammadqad;dQcdqad;reshape(dgammadqed,6*Nftot,1);reshape(dQcdqed,Nftot*Nftot,1);reshape(dgammadwLd,6*6,1);reshape(dQcdwLd,6*Nftot,1)];
    otherwise
        error('Define Correctly Settings')
end



    
end