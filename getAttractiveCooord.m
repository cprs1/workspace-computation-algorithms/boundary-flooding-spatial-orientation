%% BOUNDARY FLOODING ALGORITHM
% F.Zaccaria 02 February 2022

% function to get attractive points in WK

% INPUT:
% params: structure with simulation parameters

% OUTPUT:
% attractive_points: coordinate of attractive points in unitary space

function attractive_points = getAttractiveCooord(params)
% TO DO BETTER: use all the permutations, and not just some points
n_rad = params.n_rad;
pvect = linspace(0,1,n_rad)';
nvect = flip(pvect);
lato_1 = [pvect,zeros(n_rad,1)];
lato_2 = [ones(n_rad-1,1),pvect(2:end)];
lato_3 = [nvect(2:end),ones(n_rad-1,1)];
lato_4 = [zeros(n_rad-2,1),nvect(2:end-1)];
piano = [lato_1;lato_2;lato_3;lato_4];

z_vect = [];
for i = 1:n_rad
    z_vect = [z_vect;pvect(i)*ones(max(size(piano)),1)];
end

attractive_coord = [repmat(piano,n_rad,1),z_vect];

idx = randperm(n_rad*max(size(piano)));

attractive_points = attractive_coord(idx(1:n_rad),:);
end