%% BOUNDARY FLOODING ALGORITHM
% F.Zaccaria 02 February 2022

% boundary flooding algorithm function

% INPUTS:
% fcn = structure with objective functions
% params = structure with simulation parameters
% drawnowopts = option for plotting

% OUTPUTS:
% WK: array with workspace data
% outstruct = structure with results

function [WK,outstruct] = FCN_BoundaryFloodingSpatOrientWK(fcn,params,drawnowopts)
tic;

%% HOW WK IS ORDERED:
% 1: point idx
% 2: x coordinate
% 3: y coordinate
% 4: z coordinate
% 5: idx to identify kind of point  
% 6: n° of search direction employed
% 7: inverse cond of Jac at this point


% wk(:,4) =
% 1: point in the wk
% 2: solver fails
% 3: norm inv too high
% 4: type 1 singu
% 5: type 2 singu
% 6: change in stab
% 7: mech constr
% 8: rad vector
%% COLLECT INPUTS
y0 = params.y0;
pinit = params.pstart;
maxiter = params.maxiter;
Cordefun = fcn.objetivefcn;
options = optimoptions('fsolve','display','off','SpecifyObjectiveGradient',true,'CheckGradients',false,'Maxiter',maxiter,'Algorithm','trust-region');

%% DEFINE GRID
table = defineGrid(params);
np = numel(table(:,1));
idx = 1:1:np;
WK = [idx',table,zeros(np,3)];
guesses = zeros(max(size(y0)),np);



%% INITIALIZATION

    start_to_go_idx = getNeighbors(WK,pinit,params);
    start_to_go_idx = start_to_go_idx(1);
    
    pend = [WK(start_to_go_idx,2);WK(start_to_go_idx,3);WK(start_to_go_idx,4)];
    
    feq = @(y) Cordefun(y,pend);
    [y_start,~,~,~,~] = fsolve(feq,y0,options);
    
    % store results
    WK(start_to_go_idx,5) = 1; % in WK
    guesses(:,start_to_go_idx) = y_start; % store for future

    
%% RADIATING VECTORS

attractive_points = getAttractiveCooord(params);
blocked_points = [];
nr = 1;
n_rad = params.n_rad; % number of radiating vectors equaly spaced in 0,2\pi

while nr <= n_rad  
    wk_todo = [];
    old_points_to_go_idx = start_to_go_idx;
    y0 = guesses(:,old_points_to_go_idx);
    pstart = pinit;
    if drawnowopts == 1
        a_f = unitary2physical(attractive_points(nr,:),params);
        plot3(a_f(1),a_f(2),a_f(3),'ro')
        drawnow
    end
    % go up until border-condition occurs
    exitflag = 1;
    iter = 1;
    while exitflag==1
        dir_it = getNewDir(iter,attractive_points(nr,:),blocked_points,pstart,params,drawnowopts);
        dir_new = convert_direction(dir_it,params);
        pdes = pstart + dir_new;
        
        neighbors_idx = getNeighbors(WK,pdes,params);
        neighbors_todo_idx = neighbors_idx(WK(neighbors_idx,6)==0);

        if numel(neighbors_todo_idx)==0
            exitflag = 0 ;
        else
            point_to_go_idx = neighbors_todo_idx(1);
            pend = [WK(point_to_go_idx,2);WK(point_to_go_idx,3);WK(point_to_go_idx,4)];
            feq = @(y) Cordefun(y,pend);
            [y,~,solve_flag,~,jac] = fsolve(feq,y0,options);
            jac = full(jac);
            norminv = norm(inv(jac),Inf);
            WK(point_to_go_idx,7) = norminv;
            
            % update
            iter = iter+1;
            pstart = pdes;
            [exitflag,exitidx] = getOutCondition(y,jac,solve_flag,norminv,pdes,params,fcn);
            WK(point_to_go_idx,5) = exitidx;
            if exitflag == 1
                    WK(point_to_go_idx,6) = 0;
                    guesses(:,point_to_go_idx) = y;
                    y0 = y;
                    old_points_to_go_idx = point_to_go_idx;
                    if drawnowopts == 1
                        plot3(pend(1),pend(2),pend(3),'c.')
                        drawnow
                    end
            end
        end
    end
    
    blocked_points = [blocked_points;pend'];   
    if WK(point_to_go_idx,6) == 0 && exitidx ~= 0
        WK(point_to_go_idx,6) = nr;
        WK(point_to_go_idx,6) = nr;
        wk_todo = [point_to_go_idx,old_points_to_go_idx];
        if drawnowopts == 1
            plot3(pend(1),pend(2),pend(3),'ro','LineWidth',2)
            plot3(WK(old_points_to_go_idx,2),WK(old_points_to_go_idx,3),WK(old_points_to_go_idx,4),'bo','LineWidth',2)
            drawnow
        end
   end      
        
        
        % MAIN LOOP
        while numel(wk_todo)>0
            % EXTRACT POINT to do
            fail_idx = wk_todo(1,1);
            workspace_idx = wk_todo(1,2);
            wk_todo = wk_todo(2:end,:);
            p_fail = [WK(fail_idx,2),WK(fail_idx,3),WK(fail_idx,4)];
            pstart = [WK(workspace_idx,2),WK(workspace_idx,3),WK(workspace_idx,4)];
            %create box around fail and sort them
            [id_neigh_todo,idn2] = getFailBox(WK,p_fail,pstart,fail_idx,params);
            while numel(id_neigh_todo)>0
                % find the first to do
                current_idx = id_neigh_todo(1,1);
                pend = [WK(current_idx,2);WK(current_idx,3);WK(current_idx,4)];
                id_neigh_todo = id_neigh_todo(2:end,1);
                % find its neighbors in wk in idn2
                idx_wk =(WK(idn2,5)==1);
                idm_t = idn2(idx_wk==1);
                idm = getNeighbors(WK(idm_t,:),pend,params);
                
                if numel(idm)~=0
                    % compute
                    [~,idmin] = min(WK(idm,7));
                    id_start = idm(idmin);
                    y0 = guesses(:,id_start);
                    feq = @(y) Cordefun(y,pend);
                    [y,~,solve_flag,~,jac] = fsolve(feq,y0,options); 
                    jac = full(jac);
                    % border-conditions
                    solve_flag = ((solve_flag == 1) || (solve_flag == 2) || (solve_flag == 3) || (solve_flag == 4));
                    norminv = norm(inv(jac),Inf);
                    WK(current_idx,7) = norminv;
                    WK(current_idx,6) = nr;  
                    [exitflag,exitidx] = getOutCondition(y,jac,solve_flag,norminv,pend,params,fcn);
                    WK(current_idx,5) = exitidx;
                    if exitflag == 1
                        WK(current_idx,6) = nr;
                        guesses(:,current_idx) = y;  
                        if drawnowopts == 1
                            plot3(pend(1),pend(2),pend(3),'b.')
                            drawnow
                        end
                    else
                        new_todo = [current_idx,id_start];
                        wk_todo = [wk_todo;new_todo];
                        if drawnowopts == 1
                            plot3(pend(1),pend(2),pend(3),'r.')
                            drawnow
                        end
                    end
                end      
            end
        end
    
    nr = nr + 1;
end

%% DATA
idx_computed = WK(WK(:,7)~=0,1);
WK = WK(idx_computed ,:);
time = toc;
outstruct.time = time/60;

end