    % DERIVATIVE OF ROTATION MATRIX X WRT ITS ANGLE
function R = dRxdt(t)
R=[0,  0    ,0;
   0,-sin(t),-cos(t);
   0,cos(t),-sin(t)];
end