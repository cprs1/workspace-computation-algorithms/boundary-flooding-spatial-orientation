function flag = mechconstr6EFR_1c(y,actlim,r,E,Nf,Nleg,stresslim)

%% JOINT LIMITS
flag = 1;
i = 1;
while i<=6 && flag==1
    qa = y(i);
    if qa>actlim(1) && qa<actlim(2)
        flag = 1;
    else
        flag = 0;
    end
    i = i+1;
end
end