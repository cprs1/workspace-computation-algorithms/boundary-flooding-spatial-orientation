function [flag1,flag2] = SingularityMode6EFR_1c(jac,Nfm,y,rotparams)

Nf = 3*Nfm;

qp = y(1+6+2*6*Nf + Nf+6+6:6+2*6*Nf + Nf+6+6+6);
qd = y(1+6+2*6*Nf + Nf+6:6+2*6*Nf + Nf+6+6,1);
Dplat = rot2twist(qp(4:6,1),rotparams);
Ddisk = rot2twist(qd(4:6,1),rotparams);
jac(1+2*6*Nf+Nf+6+3  :2*6*Nf+Nf+6+6,:)   = Ddisk'*jac(1+2*6*Nf+Nf+6+3   :2*6*Nf+Nf+6+6,:); % disk equilibrium
jac(1+2*6*Nf+Nf+6+6+3:2*6*Nf+Nf+6+6+6,:) = Dplat'*jac(1+2*6*Nf+Nf+6+6+3 :2*6*Nf+Nf+6+6+6,:); % plat equilibrium

A1 = jac(1:2*6*Nf+Nf+6+6+6,1:6);
U1 = jac(1:2*6*Nf+Nf+6+6+6,1+6:6+2*6*Nf+Nf+6+6);
P1 = jac(1:2*6*Nf+Nf+6+6+6,1+6+2*6*Nf+Nf+6+6:6+2*6*Nf+Nf+6+6+6);

A2 = jac(1+2*6*Nf+Nf+6+6+6:2*6*Nf+Nf+6+6+6+5*6+4*6+5,1:6);
U2 = jac(1+2*6*Nf+Nf+6+6+6:2*6*Nf+Nf+6+6+6+5*6+4*6+5,1+6:6+2*6*Nf+Nf+6+6);
P2 = jac(1+2*6*Nf+Nf+6+6+6:2*6*Nf+Nf+6+6+6+5*6+4*6+5,1+6+2*6*Nf+Nf+6+6:6+2*6*Nf+Nf+6+6+6);

G = jac(1:2*6*Nf+Nf+6+6+6,1+6+2*6*Nf+Nf+6+6+6:6+2*6*Nf+Nf+6+6+6+5*6+4*6+5);
Z = null(G');

T1 = [Z'*A1,Z'*U1;
         A2,   U2];
     
T2 = [Z'*P1,Z'*U1; 
         P2,   U2];

flag1 = rcond(T1);
flag2 = rcond(T2);

end