%% PLOT3 OF A LOCAL FRAME
% p \in R^3, frame position \wrt inertial frame
% R \in SO(3) orientation matrix of the local frame \wrt inertial frame
% L: length factor of the arrows \in [0,1]
function plot3SdR(p,R,L)

xv = [1;0;0];
yv = [0;1;0];
zv = [0;0;1];

x = R*xv;
y = R*yv;
z = R*zv;

plot3(p(1),p(2),p(3),'o','Color','blue')
hold on
quiver3(p(1),p(2),p(3),L*x(1),L*x(2),L*x(3),'Color','red') % local x --> red
quiver3(p(1),p(2),p(3),L*y(1),L*y(2),L*y(3),'Color','blue') % local y --> blue
quiver3(p(1),p(2),p(3),L*z(1),L*z(2),L*z(3),'Color','black') % local  --> black

end