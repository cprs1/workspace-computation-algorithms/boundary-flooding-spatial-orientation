%% BOUNDARY FLOODING ALGORITHM
% F.Zaccaria 02 February 2022

% function to plot constant torsion slice
% INPUT:
% WK_full: 3d workspace
% slice coordinate: coordinate of the slice to plot

% OUTPUT: 
% h: plot identifier to clear the plot if necessary


function h = PlotTorsionSlice(WK_full,slice_coordinate)


% slice coordinate: if a XY is desired, select the Z coordinateS
idslice = 4;
id1 = 2;
id2 = 3;

%% find the correct slice among the WK
coord_values = WK_full(:,idslice);
[~,id] = min(abs(coord_values-slice_coordinate));
correct_coordinate = WK_full(id,idslice);
idok = (WK_full(:,idslice) == correct_coordinate);
WK = WK_full(idok==1,:);
WK(:,id2) = rad2deg(WK(:,id2));
%% NUMERICAL RESULT

idwk = (WK(:,5) == 1 & WK(:,6) ~=0);
idT1 = (WK(:,5) == 2) | (WK(:,5) == 3) | (WK(:,5) == 4);
idT2 = (WK(:,5) == 5) | (WK(:,5) == 6);
idmech = (WK(:,5) == 7);

% figure()
h1 = polarplot(WK(idwk,id1),WK(idwk,id2),'b.');
hold on
h2 = polarplot(WK(idT1,id1),WK(idT1,id2),'r.');
h3 = polarplot(WK(idT2,id1),WK(idT2,id2),'k.');
h4 = polarplot(WK(idmech,id1),WK(idmech,id2),'g.');

grid minor
% title('Constant Torsion Slice')
rlim([0 +90])
% %% COMPUTE ALL BOUNDARIES
% areap = zeros(n_rad,1);
% polyshapes = cell(n_rad,1);
% figure()
% for i = 1:n_rad
%     idwk_i = (WK(:,5) ~= 1 & WK(:,6) == i);
%     if numel(WK(idwk_i==1,1))>0
%         points = [WK(idwk_i,id1),WK(idwk_i,id2)];
%        [k,areap(i)] = boundary(points(:,1),points(:,2),.4);
%        if numel(k)>1
%            polyinf = polyshape(points(k,1),points(k,2));
%            polarplot(polyinf.Vertices)
%            polyshapes{i,1} = polyinf;
%            hold on
%        end
%     end
% end
% grid minor
% title('Constant Torsion Refined')
 h = [h1;h2;h3;h4];

end