%% BOUNDARY FLOODING ALGORITHM
% F.Zaccaria 02 February 2022

% function to get plots

% INPUT:
% WK: array to containt wk points
% parameters: structure with simulation parameters
% n_rad: number of radiating directions

function FCN_plot_space(WK,parameters)


boxsize = parameters.boxsize;
n_rad = parameters.n_rad;

%% NUMERICAL CYLINDRICAL PLOT
figure()
thmax = pi/2;
polar(0,thmax);
[th,rho,zo] = pol2cart(WK(:,2),WK(:,3),WK(:,4));
z_min = -pi/2;
hold on
z = zo-z_min;
WKo = [WK(:,1),th,rho,z,WK(:,5:7)];
idwk = (WKo(:,5) == 1 & WKo(:,6) ~=0);
idT1 = (WKo(:,5) == 2) | (WKo(:,5) == 3) | (WKo(:,5) == 4);
idT2 = (WKo(:,5) == 5) | (WKo(:,5) == 6);
idmech = (WKo(:,5) == 7);
plot3(WKo(idwk,2),WKo(idwk,3),WKo(idwk,4),'b.')
line([0,0],[0,0],[0,pi],'Color',[0,0,0])
plot3(WKo(idT1,2),WKo(idT1,3),WKo(idT1,4),'r.')
plot3(WKo(idT2,2),WKo(idT2,3),WKo(idT2,4),'k.')
plot3(WKo(idmech,2),WKo(idmech,3),WKo(idmech,4),'g.')
view([60 30])

%% REFINED BOUNDARY
volumep = zeros(n_rad,1);
figure()
polar(0,thmax);
hold on
for i = 1:n_rad
    idwk_i = (WK(:,5) == 1 & WK(:,6) == i );
    points = [WKo(idwk_i,2),WKo(idwk_i,3),WKo(idwk_i,4)];
    [k,volumep(i)] = boundary(points(:,1),points(:,2),points(:,3),0.99);
    trisurf(k,points(:,1),points(:,2),points(:,3),'Facecolor','cyan','FaceAlpha',0.5)
end
line([0,0],[0,0],[0,pi],'Color',[0,0,0])
view([60 30])
title('Workspace Boundary')


end