%% BOUNDARY FLOODING ALGORITHM
% F.Zaccaria 02 February 2022


%% MAIN FILE
clear
close all
clc

%% ROBOT PARAMETERS
% FILE_6RFSrobotfile
% FILE_6EFSrobotfile
% FILE_6EFRrobotfile
FILE_6EFRdisk_robotfile

%% SIMULATION PARAMETERS
FILE_inputfile

%%
instantplot = 1; % set 1 to see how it works during computation
[WK,outstruct] = FCN_BoundaryFloodingSpatOrientWK(fcn,params,instantplot);

%%

FCN_plot_space(WK,params)

%% Slice

% one slice:
figure()
torsion = 0*pi/180;
h = PlotTorsionSlice(WK,torsion);

% % slices video
% 
% myvideo = VideoWriter('Slice Video');
% myvideo.Quality = 95;
% myvideo.FrameRate = 2;
% open(myvideo);
% 
% figure()
% torsion = -90*pi/180;
% while torsion<90*pi/180
% 
%     torsion = torsion + 2*pi/180;
%     h = PlotTorsionSlice(WK,torsion);
%     title(['Costant_Torsion', num2str(torsion*180/pi),'�'])
%     
%     frame = getframe(gcf);
%     writeVideo(myvideo,frame);
%     
%     pause(0.1)
%     delete(h)
%     drawnow
% end
% 
% close(myvideo);